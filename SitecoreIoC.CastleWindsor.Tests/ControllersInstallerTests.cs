﻿using Castle.Windsor;
using NUnit.Framework;
using SitecoreIoC.Tests.Common;

namespace SitecoreIoC.CastleWindsor.Tests
{
    [TestFixture]
    public class ControllersInstallerTests
    {
        [SetUp]
        public void SetUp()
        {
            SitecoreIoCApplication.PreApplicationStart();

            var loadAssembley = CastleWindsorApplication.WindsorContainer;
        }

        [Test]
        public void CanResolveController()
        {
            var container = new WindsorContainer();

            var controllersInstaller = new ControllersInstaller();
            controllersInstaller.Install(container, null);
            Assert.That(container.Resolve<DummyController>(), Is.Not.Null);
        }

        [Test]
        public void CanResolveControllerFromAnotherAssembley()
        {
            var container = new WindsorContainer();

            var controllersInstaller = new ControllersInstaller();
            controllersInstaller.Install(container, null);
            Assert.That(container.Resolve<AnotherDummyController>(), Is.Not.Null);
        }

        [Test]
        public void GivenPreApplicationStartCalledCanResolveController()
        {
            Assert.That(CastleWindsorApplication.WindsorContainer.Resolve<DummyController>(), Is.Not.Null);
        }

        [Test]
        public void GivenPreApplicationStartCalledCanResolveControllerFromAnotherAssembley()
        {
            Assert.That(CastleWindsorApplication.WindsorContainer.Resolve<AnotherDummyController>(), Is.Not.Null);
        }

        [Test]
        public void Isolated_GivenPreApplicationStartCalledCanResolveController()
        {
            Isolated.Execute(() =>
            {
                SitecoreIoCApplication.PreApplicationStart();
                Assert.That(CastleWindsorApplication.WindsorContainer.Resolve<DummyController>(), Is.Not.Null);
            });
        }

        [Test]
        public void Isolated_GivenPreApplicationStartCalledCanResolveControllerFromAnotherAssembley()
        {
            Isolated.Execute(() =>
            {
                SitecoreIoCApplication.PreApplicationStart();
                Assert.That(CastleWindsorApplication.WindsorContainer.Resolve<AnotherDummyController>(), Is.Not.Null);
            });
        }
    }
}
