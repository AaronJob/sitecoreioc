﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using SitecoreIoC.CastleWindsor.Example.Usecases;
using SitecoreIoC.Example.Common.Usecases;

namespace SitecoreIoC.CastleWindsor.Example.Installers
{
    public class UsecaseInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IHelloFromUsecase>()
                .ImplementedBy<CastleWindsorHelloFromUsecase>()
                .LifestylePerWebRequest());
        }
    }
}