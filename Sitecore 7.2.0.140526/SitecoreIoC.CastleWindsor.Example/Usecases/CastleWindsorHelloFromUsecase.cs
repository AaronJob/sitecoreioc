﻿using SitecoreIoC.Example.Common.Model;
using SitecoreIoC.Example.Common.Usecases;

namespace SitecoreIoC.CastleWindsor.Example.Usecases
{
    public class CastleWindsorHelloFromUsecase : IHelloFromUsecase
    {
        public Hello WhoSaidHello()
        {
            return new Hello
            {
                HelloFrom = "CastleWindsorHelloFromUsecase"
            };
        }
    }
}