﻿using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace SitecoreIoC.CastleWindsor
{
    public class CastleWindsorApplication : IApplication
    {
        public static IWindsorContainer WindsorContainer;

        public void PreApplicationStart()
        {
            WindsorContainer = new WindsorContainer();

            foreach (var applicationAssembly in SitecoreIoCApplication.ApplicationAssemblies)
            {
                WindsorContainer.Install(FromAssembly.Instance(applicationAssembly));
            }
        }

        public void ApplicationShutdown()
        {
            WindsorContainer.Dispose();
        }

        public IControllerFactory GetControllerFactory()
        {
            return new CastleWindsorControllerFactory(WindsorContainer.Kernel);
        }
    }
}
