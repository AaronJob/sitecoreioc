﻿using Castle.MicroKernel.Registration;
using System.Web.Mvc;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace SitecoreIoC.CastleWindsor
{
    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            foreach (var applicationAssembly in SitecoreIoCApplication.ApplicationAssemblies)
            {
                container.Register(Classes.FromAssembly(applicationAssembly)
                .BasedOn<IController>()
                .LifestyleTransient());
            }
        }
    }
}